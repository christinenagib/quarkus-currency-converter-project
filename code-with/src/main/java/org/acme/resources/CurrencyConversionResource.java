package org.acme.resources;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.acme.models.CurrencyConversion;
import org.acme.services.CurrencyConversionService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.io.IOException;

@Path("/currency/result")
public class CurrencyConversionResource {
    @RestClient
    CurrencyConversionService currencyConversionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/from={fromCurrency}&to={toCurrency}&amount={amount}")
    public Uni<CurrencyConversion> getResult(@PathParam("fromCurrency") String fromCurrency, @PathParam("toCurrency") String toCurrency, @PathParam("amount") float amount) throws IOException, InterruptedException {

        Uni<CurrencyConversion> result = currencyConversionService.getCurrencyResult(fromCurrency, toCurrency, amount);
        Uni<CurrencyConversion> multiplyCurrencyResult = currencyConversionService.getCurrencyResult(fromCurrency, toCurrency, amount*2);
        Uni<CurrencyConversion> switchResult = currencyConversionService.getCurrencyResult(toCurrency, fromCurrency, amount);
        return multiplyCurrencyResult;

    }

}
