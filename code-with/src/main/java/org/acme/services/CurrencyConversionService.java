package org.acme.services;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Singleton;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.QueryParam;
import org.acme.models.CurrencyConversion;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Singleton
@RegisterRestClient
@ClientHeaderParam(name = "apikey", value = "m62JZp6QKN05VIUj2Fz6jymiRvpCcPc9")
public interface CurrencyConversionService {

    @GET
    @Consumes("application/json")
    public Uni<CurrencyConversion> getCurrencyResult(@QueryParam("from") String fromCurrency, @QueryParam("to") String toCurrency, @QueryParam("amount") float amount);

}
